import React from 'react'
import {
	StyleSheet,
	Text,
	View,
	Image,
	TouchableOpacity,
	ImageBackground,
	SafeAreaView,
	Platform,
	StatusBar
} from 'react-native'
import WebViewScreen from './components/webview'

class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isWebView: false,
			webviewURL: 'https://www.google.com/',
			isFontLoaded: false
		}
		this.handleNavigation = this.handleNavigation.bind(this)
		this.reset = this.reset.bind(this)
	}

	async componentWillMount() {

		this.setState({ isFontLoaded: true })
	}

	componentDidMount() {
		StatusBar.setBackgroundColor('#3700B3')
	}

	reset() {
		this.setState({
			isWebView: false
		})
	}

	handleNavigation = url => {
		this.setState({
			isWebView: true,
			webviewURL: url
		})
	}
	render() {
		console.log('render')
		if (!this.state.isFontLoaded) {
			return <Text>Loading</Text>
		} else {
			if (this.state.isWebView) {
				return (
					<SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
						<StatusBar backgroundColor="#3700B3" hidden={false} />
						<WebViewScreen
							url={this.state.webviewURL}
							reset={this.reset}
							key={this.state.webviewURL}
						/>
					</SafeAreaView>
				)
			} else {
				return (
					<SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
						<StatusBar
							backgroundColor="#3700B3"
							barStyle="dark-content"
							hidden={false}
							translucent={true}
						/>
						<ImageBackground
							source={require('./assets/doodle_background.png')}
							style={styles.mainbackgroundColor}
						>
							<View style={styles.header}>
								<Image
									source={require('./assets/voice_health_assistant_primary.png')}
									style={styles.header_mic}
								/>
								<Text style={styles.headerText}>
									Voice Health Assitant.
								</Text>
							</View>
							<View style={styles.aSection}>
								<Image
									style={styles.avtar}
									source={require('./assets/avtar.png')}
								/>

								<Text
									style={{ color: '#353535', fontFamily: 'Montserrat' }}
								>
									<Text  style={styles.greetingText}>Hi there 👋 </Text>
									{'\n'}
									I'm Sarah, and here to help you connect to the right
									doctor for your health condition
								</Text>
							</View>
							<View style={styles.bSection}>
								<Text style={{ fontFamily: 'Montserrat' }}>
									Please choose your hospital to start speaking to me
								</Text>
								<View style={styles.hospitalBadgeWrapper}>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/apollo.html'
											)
										}
									>
										<View style={styles.hospitalBadge}>
											<Image
												style={styles.hospitalLogo}
												source={require('./assets/apollo.png')}
											/>
											<Text style={styles.hospitalText}>
												Apollo Hospital
											</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/medanta.html'
											)
										}
									>
										<View style={styles.hospitalBadge}>
											<Image
												style={styles.hospitalLogo}
												source={require('./assets/medanta.png')}
											/>
											<Text style={styles.hospitalText}>
												Medanta Hospital
											</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/columbia_asia.html'
											)
										}
									>
										<View style={styles.hospitalBadge}>
											<Image
												style={styles.hospitalLogo}
												source={require('./assets/columbia_asia.png')}
											/>
											<Text style={styles.hospitalText}>
												Columbiasia Hospital
											</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/kims_global.html'
											)
										}
									>
										<View style={styles.hospitalBadge}>
											<Image
												style={styles.hospitalLogo}
												source={require('./assets/kims.jpg')}
											/>
											<Text style={styles.hospitalText}>
												Kims Hospital
											</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/coming_soon.html'
											)
										}
									>
										<View style={styles.hospitalBadge}>
											<Image
												style={styles.hospitalLogo}
												source={require('./assets/fortis_logo.png')}
											/>
											<Text style={styles.hospitalText}>
												Fortis Hospital
											</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/coming_soon.html'
											)
										}
									>
										<View style={styles.hospitalBadge}>
											<Image
												style={styles.hospitalLogo}
												source={require('./assets/Kauvery_Hospital_logo.png')}
											/>
											<Text style={styles.hospitalText}>
												Kauvery Hospital
											</Text>
										</View>
									</TouchableOpacity>
								</View>
							</View>
						</ImageBackground>
					</SafeAreaView>
				)
			}
		}
	}
}
export default App

const styles = StyleSheet.create({
	mainbackgroundColor: {
		flex: 1,
		paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
	},
	avtar: {
		width: 80,
		height: 80,
		borderRadius: 100,
		marginRight: 10
	},
	header: {
		height: 50,
		display: 'flex',
		flexDirection: 'row',
		alignContent: 'center',
		backgroundColor: '#6002ee',
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: 16,
		paddingRight: 16
	},
	headerText: {
		fontSize: 20,
		color: 'white',
		lineHeight: 50,
		fontFamily: 'Montserrat'
	},
	header_mic: {
		height: 30,
		width: 30,
		backgroundColor: 'white',
		borderRadius: 100,
		padding: 5,
		marginRight: 10,
		marginTop: 10
	},
	aSection: {
		display: 'flex',
		flexDirection: 'row',
		backgroundColor: 'rgba(3, 218, 198, 0.5)',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 25,
		paddingBottom: 25,
		paddingLeft: 80,
		paddingRight: 80
	},
	greetingText: {
		marginBottom: 8,
		fontSize: 16,
		fontWeight: '500',
		color: '#202020'
	},
	bSection: {
		padding: 16
	},
	hospitalLogo: {
		height: 40,
		width: 40,
		borderRadius: 100,
		backgroundColor: '#ffffff',
		padding: 5,
		margin: -10,
		borderWidth: 1,
		borderColor: '#03dac6',
		marginRight: 10
	},
	hospitalText: {
		fontSize: 14,
		fontFamily: 'Montserrat'
	},
	hospitalBadge: {
		display: 'flex',
		flexDirection: 'row',
		backgroundColor: 'white',
		shadowColor: 'black',
		shadowOpacity: 0.4,
		elevation: 4,
		margin: 5,
		borderRadius: 25,
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: 10,
		paddingRight: 10,
		height: 40,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	hospitalBadgeWrapper: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	}
})
