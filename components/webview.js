import * as React from 'react'
import { BackHandler, Text } from 'react-native'
import { WebView } from 'react-native-webview'
import ProgressWebView from 'react-native-progress-webview'

export default class WebViewScreen extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			shouldGoBack: false
		}
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
	}

	componentWillMount() {
		BackHandler.addEventListener(
			'hardwareBackPress',
			this.handleBackButtonClick
		)
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			'hardwareBackPress',
			this.handleBackButtonClick
		)
	}

	handleBackButtonClick() {
		this.props.reset()
		return true
	}

	render() {
		console.log("URL", this.props.url)
		return (
				<ProgressWebView
					source={{ uri: this.props.url }}
					javaScriptEnabled={true}
					domStorageEnabled={true}
					useWebKit={true}
					allowsInlineMediaPlayback
					color={'#03DAC6'}
					height={10}
					startInLoadingState={true}
					androidHardwareAccelerationDisabled={true}
				/>
		)
	}
}
