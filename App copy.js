import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import * as Font from 'expo-font'
import WebViewScreen from './components/webview'

class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isWebView: false,
			webviewURL: 'https://www.google.com/',
			isFontLoaded: false
		}
		this.handleNavigation = this.handleNavigation.bind(this)
		this.reset = this.reset.bind(this)
	}

	async componentWillMount() {
		await Font.loadAsync({
			Montserrat: require('./assets/fonts/Montserrat-Regular.ttf')
		})
		this.setState({ isFontLoaded: true })
	}

	reset() {
		this.setState({
			isWebView: false
		})
	}

	handleNavigation = url => {
		this.setState({
			isWebView: true,
			webviewURL: url
		})
	}
	render() {
		console.log('render')
		if (!this.state.isFontLoaded) {
			return <Text>Loading</Text>
		} else {
			if (this.state.isWebView) {
				return (
					<WebViewScreen
						url={this.state.webviewURL}
						key={this.state.webviewURL}
						reset={this.reset}
					/>
				)
			} else {
				return (
					<View style={styles.background}>
						<View style={styles.container}>
							<Text style={styles.title}>Voice Health assistant</Text>
							<View style={styles.wrapper}>
								<View style={styles.grid_a}>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/test.html'
											)
										}
									>
										<View style={styles.icon}>
											<Image
												source={require('./assets/apollo.png')}
												style={styles.hospital_logo}
											/>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/columbia_asia.html'
											)
										}
									>
										<View style={styles.icon}>
											<Image
												source={require('./assets/columbia_asia.png')}
												style={styles.hospital_logo}
											/>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/coming_soon.html'
											)
										}
									>
										<View style={styles.icon}>
											<Image
												source={require('./assets/fortis_logo.png')}
												style={styles.hospital_logo}
											/>
										</View>
									</TouchableOpacity>
								</View>
								<View style={styles.grid_b}>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/medanta.html'
											)
										}
									>
										<View style={styles.icon}>
											<Image
												source={require('./assets/medanta.png')}
												style={styles.hospital_logo}
											/>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/kims_global.html'
											)
										}
									>
										<View style={styles.icon}>
											<Image
												source={require('./assets/kims.jpg')}
												style={styles.hospital_logo}
											/>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() =>
											this.handleNavigation(
												'https://voicehealthassistant.praktice.xyz/coming_soon.html'
											)
										}
									>
										<View style={styles.icon}>
											<Image
												source={require('./assets/Kauvery_Hospital_logo.png')}
												style={styles.hospital_logo}
											/>
										</View>
									</TouchableOpacity>
								</View>
							</View>
						</View>
					</View>
				)
			}
		}
	}
}
export default App

const styles = StyleSheet.create({
	background: {
		backgroundColor: '#ededf3',
		margin: 0,
		padding: 0,
		width: '100%',
		height: '100%'
	},
	container: {
		height: '100%',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	wrapper: {
		width: '100%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	hospital_logo: {
		width: 80,
		height: 80
	},
	title: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		fontSize: 16,
		color: '#6145bf',
		marginBottom: 35,
		fontFamily: 'Montserrat'
	},
	icon: {
		margin: 16,
		shadowOffset: { width: 0, height: 4 },
		shadowColor: 'black',
		shadowOpacity: 0.4,
		elevation: 4,
		borderRadius: 15,
		backgroundColor: 'white',
		overflow: 'hidden',
		padding: 5
	}
})
